const gulp = require('gulp');
const terser = require('gulp-terser');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const angularSettings = require('./angular.json');

gulp.task('replaceKendoCss', function () {
  const deployUrl = angularSettings.projects['digital-clone-fe-login'].architect.build.configurations.production.deployUrl;
  return gulp.src('./dist/**/*.{html,js,css}')
    .pipe(replace(' k--', ' revert--'))
    .pipe(replace('k-icon', 'revert-icon'))
    .pipe(replace('.k-', '.dcp-login-k-'))
    .pipe(replace(' k-', ' dcp-login-k-'))
    .pipe(replace('!k-', '!dcp-login-k-'))
    .pipe(replace('\'k-', '\'dcp-login-k-'))
    .pipe(replace('`k-', '`dcp-login-k-'))
    .pipe(replace('"k-', '"dcp-login-k-'))
    .pipe(replace('k-${', 'dcp-login-k-${'))
    .pipe(replace('dcp-login-dcp-login-k-', 'dcp-login-k-'))
    .pipe(replace(' revert--', ' k--'))
    .pipe(replace('revert-icon', 'k-icon'))
    .pipe(replace('url("kendo-font-icons.ttf")', `url("${deployUrl}/kendo-font-icons.ttf")`))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('copyFontLocal', () => {
  return gulp.src('./src/fonts-local.scss')
    .pipe(rename('fonts.scss'))
    .pipe(gulp.dest("./src"));
});

gulp.task('copyFontBuild', () => {
  return gulp.src('./src/fonts-build.scss')
    .pipe(rename('fonts.scss'))
    .pipe(gulp.dest("./src"));
});

gulp.task('incrementBuildNumber', async function() {
    const buildNumber = parseInt(require('./package.json').buildNumber);
    gulp.src("./package.json")
        .pipe(jeditor({
            'buildNumber': buildNumber + 1 + ''
        }))
        .pipe(gulp.dest("./"));
});

gulp.task('minifyBuild', function () {
    return gulp.src('./dist/*.js')
        .pipe(terser({
          parse: {
            // parse options
          },
          compress: {

          },
          format: {
            comments: false
          },
          toplevel: true
        }))
        .pipe(gulp.dest('./dist'));
});
