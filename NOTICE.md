# NOTICE

This project is licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:

    http://www.apache.org/licenses/LICENSE-2.0

## Additional Terms:

This project includes branding specific to F. Hoffmann-La Roche Ltd ("Roche").
When redistributing or using this project, you must use the provided customizer tool
to remove Roche-specific branding elements that pertain solely to the appearance
of the frontend user interface. These branding elements may include logos, colors,
fonts, and other visual aspects associated with Roche's identity.

The customizer tool does not alter any functional aspects of the project and is intended
only to modify visual elements to ensure compliance with Roche's branding policies.
The customizer tool can be found at:

    https://gitlab.com/roche/dcp/platform/frontend/customizer

By using this project, you agree to apply the customizer tool to remove Roche-specific
branding elements before distribution or use in a production environment.

This requirement does not alter the terms of the Apache License 2.0, but adds an
additional condition for the use and redistribution of this project in its branded form.

---

F. Hoffmann-La Roche Ltd