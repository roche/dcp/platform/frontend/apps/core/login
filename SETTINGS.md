The structure for settings.json file should include this attributes

```
{
  // Used in environment files
  "production": false, // Used during ng build process
  "apiUrl": "https://api-domain.com", // Current application BE AOU URL like "https://domain.com"
  "baseAppUrl": "https://domain.com/", // Base app deploy url (with trailing slash) like "https://domain.com/"
  
  // Used in Login page html
  "privacyPolicyUrl": "https://domain.com/privacy_policy.htm" // Url to applicable privacy policy
  
  // Used in Actions Scope Interceptor
  "backendPortCheckUrl": "domain.com:", // Needs to be in format "domain.com:" so we can use it in interceptor, to assume if there is an port part of the url  
}
``
