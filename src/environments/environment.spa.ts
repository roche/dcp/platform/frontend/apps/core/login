// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// move env vars to constants

import {apiMicroservicesTypes} from '../libs/dc-core/src/core/apiMicroservicesTypes';

declare var require: any;
const packageJson = require('../../package.json');
const settings = require('../../settings.json');

export const environment: {[key: string]: any | any[], filterMicroservice: apiMicroservicesTypes} = {
  filterMicroservice: 'basic',
  production: settings.production,
  angularVersion: packageJson.dependencies["@angular/common"].split('.')[0],
  userMockApi: false,
  apiUrl: settings.apiUrl,
  menuTranslationsUrl: settings.baseAppUrl,
  apiServiceUrl: settings.apiServiceUrl,
  translationsUrl: 'http://localhost:4200/assets/',
  version: `${packageJson.version}.${packageJson.buildNumber}`,
  isAppValidated: true,
  appName: 'Login',
  domains: [
    'EMEA',
    'NALA',
    'ASIA',
    'EXBP',
  ],
  defaultRefreshInterval: {
    interval: 30,
    type: 's'
  },
  staticThreshold: 5,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
