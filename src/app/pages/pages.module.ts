import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  DcSharedModule,
} from '@digital-clone-fe/dc-shared';
import { LoginComponent } from './login/login.component';
import { SsoComponent } from './sso/sso.component';
import { VersionsComponent } from './versions/versions.component';
export { LoginComponent } from './login/login.component';
export { SsoComponent } from './sso/sso.component';

@NgModule({
  imports: [CommonModule, DcSharedModule],
  declarations: [
    LoginComponent,
    SsoComponent,
    VersionsComponent,
  ],
  exports: [
    LoginComponent,
    SsoComponent,
    VersionsComponent,
  ]
})
export class PagesLoginModule {}
