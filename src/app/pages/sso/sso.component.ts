import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Login, Logout, PopulateLogoutLink, UserService} from '@digital-clone-fe/dc-user';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {NgxPermissionsService} from 'ngx-permissions';
import {DcPermissionsService, SharedConstants} from '@digital-clone-fe/dc-shared';
import {AppState, getState} from "@digital-clone-fe/dc-store/models/app.state";
import {UiFacade} from "@digital-clone-fe/dc-store/ui.facade";
import {ChangeSiteContext, LoadingUI, PopulateRedirectTo} from "@digital-clone-fe/dc-store/actions/ui.actions";

@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
  styleUrls: ['./sso.component.scss']
})
export class SsoComponent implements OnInit, OnDestroy {

  public subscription: Subscription = new Subscription();

  loginForm = {
    domain: '',
    username: '',
    password: '',
    notGMP: true
  };

  noGuid = false;

  constructor(
    private readonly store: Store<AppState>,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly permissionsService: NgxPermissionsService,
    private readonly dcPermissionsService: DcPermissionsService,
    private readonly userService: UserService,
    private readonly uiFacade: UiFacade
  ) { }

  ngOnInit(): void {
    this.loginForm.username = this.route.snapshot.params.guid;
    if (this.loginForm.username === 'notauthorized') {
      this.noGuid = true;
    } else {
      this.store.dispatch(new LoadingUI('Login'));
      this.store.dispatch(new Login(this.loginForm));

      this.subscription.add(
        this.store.pipe(select(state => state.user.current.access_token)).subscribe((token) => {
          if (!token) {
            return;
          }

          this.uiFacade.setLoading('getUserLastLoadedSite');
          this.userService.getUserLastLoadedSite().toPromise()
            .then(res => {
              const siteId = res.SiteId;
              if (!siteId) {
                const sites = getState(this.store).user.current.sites;
                if (sites[0]?.SiteId) {
                  const newSiteId = sites[0].SiteId;
                  this.triggerChangeSite(newSiteId);
                  return;
                }
                // If there is no any Sites for user trigger redirect
                this.redirectAfterLogin();
                return;
              }

              const currentSite = getState(this.store).ui.siteContext;
              if (siteId !== currentSite) {
                this.triggerChangeSite(siteId);
                return;
              }

              this.redirectAfterLogin();
              return;
            })
            .finally(() => {
              this.uiFacade.setLoaded('getUserLastLoadedSite');
            });
        })
      );
    }

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  redirectAfterLogin(): void {
    this.dcPermissionsService.initPermissions().then( () => {
      const permissionsObj = this.permissionsService.getPermissions();
      const permissions = Object.keys(permissionsObj);
      // Check if there is a redirectTo url saved
      const redirectUrl = getState(this.store).ui.redirectTo;
      const siteContext = getState(this.store).ui.siteContext;

      // If Redirect URL include '/app/' then this is URL with siteContext in it
      if (redirectUrl.includes(SharedConstants.appUrlSection) && !redirectUrl.includes(siteContext.toString())) {
        if (permissions.length === 1 && permissions[0] === 'VIEWER') {
          this.router.navigate([SharedConstants.appUrlSection + siteContext + '/continuous-data-monitoring']);
        } else if (permissions.length === 1 && permissions[0] === 'GLOBALADMIN') {
          this.router.navigate([SharedConstants.appUrlSection + siteContext + '/administration']);
        } else {
          this.router.navigate([SharedConstants.appUrlSection + siteContext + SharedConstants.homeUrlSection]);
        }
        return;
      }

      this.store.dispatch(new PopulateRedirectTo(''));
      this.router.navigate([redirectUrl.split('?')[0]], { queryParams: this.sortParams(redirectUrl)});
    });
  }

  triggerChangeSite(siteId: number) {
    this.store.dispatch(new ChangeSiteContext(siteId));
    this.uiFacade.setLoading('changeSite');
    this.userService.changeSite(siteId).toPromise()
      .then(res => {
        this.uiFacade.setSiteContext(siteId);
        this.redirectAfterLogin();
      })
      .finally(() => {
        this.uiFacade.setLoaded('changeSite');
      });
  }

  sortParams(url) {
    const queryParams = url.split('?')[1] || '';
    const params = queryParams.split('&');
    let pair = null;
    const data = {};
    params.forEach((d) => {
      pair = d.split('=');
      data[`${pair[0]}`] = pair[1];
    });
    return data;
  }

  goToLogin(): void {
    this.store.dispatch(new LoadingUI('Logout'));
    this.store.dispatch(new Logout());

    this.subscription.add(
      this.store.pipe(select(state => state.user.logoutLink)).subscribe( logoutLink => {
        if (logoutLink && logoutLink.length > 0) {
          this.store.dispatch(new PopulateLogoutLink());
          window.location.href = logoutLink;
        }
      })
    );
  }
}
