import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {UntypedFormBuilder} from '@angular/forms';
import {AppConfig} from '@digital-clone-fe/dc-core';
import {TranslateService} from '@ngx-translate/core';
import {DownloadService, RVersion} from '@digital-clone-fe/dc-shared';
import {Subscription} from 'rxjs';

import {AppState} from "@digital-clone-fe/dc-store/models/app.state";

@Component({
  selector: 'app-digital-clone-fe-versions',
  templateUrl: './versions.component.html',
  styleUrls: ['./versions.component.scss']
})
export class VersionsComponent implements OnInit, OnDestroy {

  public subscription: Subscription = new Subscription();

  objectKeys = Object.keys;

  rVersions: RVersion[] = [];

  BEVersions: any = [];

  currentVersions: {
    [key: string]: string;
  } = {};

  currentModules = Object.keys(this.currentVersions);

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router,
    private readonly formBuilder: UntypedFormBuilder,
    private readonly appConfig: AppConfig,
    public readonly translate: TranslateService,
    private readonly downloadService: DownloadService
  ) { }

  ngOnInit(): void {

    this.synchronizeWithStore();

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  synchronizeWithStore() {

    this.downloadService.readTextFile(`${this.appConfig.config.apiUrl}/fe-module-versions.json?v=${new Date().getTime()}`, text => {
      if (text) {
        const data = JSON.parse(text);
        this.currentVersions = data;
        this.currentModules = Object.keys({...this.currentVersions});
      }
    });

    this.subscription.add(
      this.store.pipe(select(state => state.ui.rVersions)).subscribe( rVersion => {
        this.rVersions = rVersion;
      })
    );

    this.subscription.add(
      this.store.pipe(select(state => state.ui.BEVersions)).subscribe( BEVersions => {
        this.BEVersions = Object.keys(BEVersions).map( key => {
          return {
            Name: key,
            Version: BEVersions[key]
          };
        });
      })
    );
  }
}
