import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AppConfig } from '@digital-clone-fe/dc-core';
import {DcPermissionsService, SharedConstants} from '@digital-clone-fe/dc-shared';
import { GetSSOLink, Login, PopulateSSOLink } from '@digital-clone-fe/dc-user';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { Subscription, Observable } from 'rxjs';
import {skip, take} from 'rxjs/operators';
import { VersionsComponent } from '../versions/versions.component';
import {environment} from '../../../environments/environment';
import settings from '../../../../settings.json';
import {AppState, getState} from "@digital-clone-fe/dc-store/models/app.state";
import {
  ChangeSiteContext,
  ClearLoading,
  GetAPIBEVersionPackage,
  GetBEVersionPackage,
  GetRVersionPackage,
  LoadingUI,
  PopulateBEVersionPackage, PopulateRedirectTo, SetLanguage
} from "@digital-clone-fe/dc-store/actions/ui.actions";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: UntypedFormGroup;
  domainList: any[] = [];
  privacyPolicyUrl = settings.privacyPolicyUrl;
  submitted = false;
  public subscription: Subscription = new Subscription();
  accessToken$: Observable<string>;
  loading$: Observable<any>;
  assetsPath: string = environment.translationsUrl;
  baseAppPath: string = environment.menuTranslationsUrl;


  notGMPChecked: boolean = true;

  ssoLink = '';

  redirectToSso = true;

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router,
    private readonly formBuilder: UntypedFormBuilder,
    private readonly appConfig: AppConfig,
    public translate: TranslateService,
    private readonly permissionsService: NgxPermissionsService,
    private readonly dcPermissionsService: DcPermissionsService,
    private _bottomSheet: MatBottomSheet
  ) {

    this.store.pipe(select(state => state.ui.language)).subscribe(lang => {
      if (lang) {
        this.langChange(lang);
      } else {
        this.langChange('en');
      }
    });
  }

  ngOnInit() {
    this.store.dispatch(new LoadingUI('GetRVersionPackage'));
    this.store.dispatch(new GetRVersionPackage());
    this.store.dispatch(new PopulateSSOLink(''));
    this.store.dispatch(new PopulateBEVersionPackage({}, true));

    const beModules = this.appConfig.config.apiMicroservicesPorts;

    Object.keys(beModules).forEach((module) => {
      // TODO: remove ppm check for dev server
      if (module !== 'old' && module !== 'ppm' && module !== 'api') {
        this.store.dispatch(new GetBEVersionPackage(module));
      }
    });

    this.store.dispatch(new GetAPIBEVersionPackage());

    if (window.location.hostname !== 'localhost') {
      this.store.dispatch(new LoadingUI('GetSSOLink'));
      this.store.dispatch(new GetSSOLink());
    }

    this.permissionsService.flushPermissions();
    this.domainList = this.appConfig.config.domains;
    this.store.dispatch(new ClearLoading());
    this.loginForm = this.formBuilder.group({
      domain: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required]],
      notGMP: [true, [Validators.required]]
    });
    this.accessToken$ = this.store.pipe(select(state => state.user.current.access_token));

    this.loading$ = this.store.pipe(select(state => state.ui.loading));
    this.subscription.add(
      this.accessToken$.subscribe((token) => {
        this.submitted  = false;
        if (token) {
          // On first login user will not have set ui.siteContext, so permissions can't be populated
          // Check if there is a siteContext, otherwise take user.sites[0].SiteId and trigger ChangeSiteContext
          const siteId = getState(this.store).ui.siteContext;
          if (siteId > 0) {
            this.redirectAfterLogin();
          } else {
            const sites = getState(this.store).user.current.sites;
            if (sites[0]?.SiteId) {
              const newSiteId = sites[0].SiteId;
              this.store.dispatch(new ChangeSiteContext(newSiteId));
              this.subscription.add(
                this.store
                  .pipe(select((state) => state.ui.siteContext))
                  .pipe(skip(1), take(1))
                  .subscribe((siteContext) => {
                    this.redirectAfterLogin();
                  })
              );
            } else {
              // If there is no any Sites for user trigger redirect
              this.redirectAfterLogin();
            }
          }
        }
      })
    );

    this.subscription.add(
      this.loading$.subscribe((result) => {
        this.submitted  = Object.keys(result).length > 0;
      })
    );

    this.subscription.add(
      this.store
        .pipe(select((state) => state.user.ssoLink))
        .subscribe((link) => {
          this.ssoLink = link;
        })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  login() {
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      this.loginForm.get('domain').markAsDirty();
      this.loginForm.get('domain').updateValueAndValidity();
      this.loginForm.get('username').markAsDirty();
      this.loginForm.get('username').updateValueAndValidity();
      this.loginForm.get('password').markAsDirty();
      this.loginForm.get('password').updateValueAndValidity();
      this.loginForm.get('notGMP').markAsDirty();
      this.loginForm.get('notGMP').updateValueAndValidity();
      return;
    }

    if (!this.submitted) {
      this.store.dispatch(new LoadingUI('Login'));
      this.store.dispatch(new Login(this.loginForm.value, false));
    }
  }

  langChange(e) {
    this.store.dispatch(new SetLanguage(e));
  }

  GMPDisclaimerChange() {
    this.notGMPChecked = this.loginForm.value.notGMP;
  }

  redirectAfterLogin() {
    this.dcPermissionsService.initPermissions().then( () => {
      const permissionsObj = this.permissionsService.getPermissions();
      const permissions = Object.keys(permissionsObj);
      // Check if there is a redirectTo url saved
      const redirectUrl = getState(this.store).ui.redirectTo;
      if (redirectUrl) {
        this.store.dispatch(new PopulateRedirectTo(''));
        this.router.navigate([redirectUrl]);
      } else {
        const siteContext = getState(this.store).ui.siteContext;
        if (permissions.length === 1 && permissions[0] === 'VIEWER') {
          this.router.navigate([
            SharedConstants.appUrlSection + siteContext + '/continuous-data-monitoring',
          ]);
        } else if (permissions.length === 1 && permissions[0] === 'GLOBALADMIN') {
          this.router.navigate([SharedConstants.appUrlSection + siteContext + '/administration']);
        } else {
          const redirectModule = this.dcPermissionsService.getRedirectLink();
          if (redirectUrl !== null) {
            this.router.navigate([`/app/${siteContext}/${redirectModule}`]);
          } else {
            this.router.navigate([SharedConstants.appUrlSection + siteContext + SharedConstants.homeUrlSection]);
          }
        }
      }
    });
  }

  signInSSO() {
    window.location.href = this.ssoLink;
  }

  cancelRedirectSSO() {
    this.redirectToSso = false;
  }

  openBottomSheet(): void {
    this._bottomSheet.open(VersionsComponent);
  }
}
