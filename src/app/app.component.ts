import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import {AlertsService} from '@digital-clone-fe/dc-shared';
import {Router} from '@angular/router';
import {DcPermissionsService} from '@digital-clone-fe/dc-shared';
import { Angulartics2, Angulartics2GoogleGlobalSiteTag } from 'angulartics2';
import {DeviceDetectorService} from 'ngx-device-detector';
import {Location} from '@angular/common';
import {AppBaseComponent} from '@digital-clone-fe/dc-app';
import {AppState} from "@digital-clone-fe/dc-store/models/app.state";
import {UiFacade} from "@digital-clone-fe/dc-store/ui.facade";

@Component({
  selector: 'app-login-root',
  templateUrl: '../libs/dc-app/src/app/app.component.html',
  styleUrls: ['../libs/dc-app/src/app/app.component.scss']
})
export class AppComponent extends AppBaseComponent implements OnInit, OnDestroy {

  constructor(
    public store: Store<AppState>,
    public translate: TranslateService,
    public alertService: AlertsService,
    public dcPermissionsService: DcPermissionsService,
    public router: Router,
    public location: Location,
    public angulartics2: Angulartics2,
    public angulartics2GoogleGlobalSiteTag: Angulartics2GoogleGlobalSiteTag,
    public deviceService: DeviceDetectorService,
    public uiFacade: UiFacade,
  ) {
    super(store, translate, alertService,
      dcPermissionsService, router, location, angulartics2,
      angulartics2GoogleGlobalSiteTag, deviceService, uiFacade);
  }

  ngOnInit() {
    try {
      document.getElementById('loader').style.display = 'none';
    } catch (e) {}

    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();

    try {
      // Fix for Angular 9 + SingleSpa. Remove when the issue is fixed: https://github.com/single-spa/single-spa-angular/issues/130
      // Solution (seems to work for now): Cleanup the DOM manually on destroy.
      const appName = '@dcp/login';
      const element = document.getElementById(`single-spa-application:${appName}`);
      element.innerHTML = '';
      document.getElementById('loader').style.display = 'flex';
    } catch (e) {}

    try {
      // Remove notifications create by this module, otherwise they will not be removable anymore
      const kendoNotifications = document.getElementsByTagName('kendo-notification-container')[0];
      if (kendoNotifications && kendoNotifications.parentNode) {
        kendoNotifications.parentNode.removeChild(kendoNotifications);
      }
    } catch (e) {}
  }
}
