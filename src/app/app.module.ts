import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ICON_SETTINGS} from '@progress/kendo-angular-icons';
import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DcSharedModule} from '@digital-clone-fe/dc-shared';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {EmptyRouteComponent} from './empty-route/empty-route.component';
import {NgxPermissionsModule} from 'ngx-permissions';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {NgIdleKeepaliveModule} from '@ng-idle/keepalive';
import {Angulartics2Module} from 'angulartics2';
import {UserStoreModule} from '@digital-clone-fe/dc-user';
import {CoreModule} from '@digital-clone-fe/dc-core';
import {InfoModalComponent} from '@digital-clone-fe/dc-app';
import {MultiTranslateHttpLoader} from 'ngx-translate-multi-http-loader';
import {environment} from '../environments/environment';
import player from 'lottie-web/build/player/lottie_svg';
import {UiFacade} from "../libs/dc-store/src/store/ui.facade";
import {PagesLoginModule} from "./pages/pages.module";
import {DcStoreLoginModule} from "@digital-clone-fe/dc-store/dc.store.login.module";
import {provideLottieOptions} from "ngx-lottie";

// Note we need a separate function as it's required
// by the AOT compiler.
export function playerFactory() {
  return player;
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: `${environment.translationsUrl}/i18n/`, suffix: '.json'},
    {prefix: `${environment.translationsUrl}/translations/`, suffix: '-login.json'},
  ]);
}

@NgModule({
  declarations: [
    AppComponent,
    InfoModalComponent,
    EmptyRouteComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    DcSharedModule,
    DcStoreLoginModule,
    UserStoreModule,
    PagesLoginModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgIdleKeepaliveModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    Angulartics2Module.forRoot()
  ],
  providers: [
    UiFacade,
    {
      provide: ICON_SETTINGS,
      useValue: {
        type: 'font'
      },
    },
    provideLottieOptions({
      player: () => player,
    }),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
